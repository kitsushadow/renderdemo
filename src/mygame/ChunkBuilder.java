/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import com.jme3.math.Vector3f;
import com.jme3.math.Vector4f;
import java.util.Random;

/**
 *
 * @author
 * kitsu
 */
public class ChunkBuilder implements java.io.Serializable{
    
    public Vector4f[] chunk = new Vector4f[25600];
    public int intX;
    public int intY;
    public int intZ;
    Random rand = new Random();
    
    //public Vector3f chunkPos = new Vector3f(0,0,0);
    
    public Vector4f[] makeChunk(Vector3f chunkPos){
        
        int chunkI=-1;
        for(int y=0;y<100;y++){
            for(int z=0;z<16;z++){
                for(int x=0;x<16;x++){
                    chunkI++;
                    if(chunkI<400+rand.nextInt(300)){
                        chunk[chunkI]=new Vector4f((float)x+chunkPos.x, (float)y+chunkPos.y, (float)z+chunkPos.z, 1);
                        //System.out.println(chunkArray[chunkI]);
                    } else {
                        chunk[chunkI]=new Vector4f((float)x+chunkPos.x, (float)y+chunkPos.y, (float)z+chunkPos.z, 0);
                    }
                }
            }
        }
        
        return chunk;
    }
    
    
    
    
    
    //public transient Vector4f[] newChunk = new Vector4f[4096];
    //public transient Vector3f chunkPos = new Vector3f(0,0,0);
    
    /*public Vector4f[] makeChunk(Vector4f[] chunk){
        int chunkI=-1;
        for(int y=0;y<16;y++){
            for(int z=0;z<16;z++){
                for(int x=0;x<16;x++){
                    chunkI++;
                    if(chunkI<1300){
                        chunk[chunkI]=new Vector4f((float)x+chunkPos.x, (float)y+chunkPos.y, (float)z+chunkPos.z, 1);
                        //System.out.println(chunkArray[chunkI]);
                    } else {
                        chunk[chunkI]=new Vector4f((float)x+chunkPos.x, (float)y+chunkPos.y, (float)z+chunkPos.z, 0);
                    }
                }
            }
        }
        return chunk;
    }*/
    
}