/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

/**
 *
 * @author
 * kitsu
 */ 
 

public class Comments {
    /*
     * 
     *  /*
     * 
     * 
         private void cameraUpdate(){
         if(cam.getDirection().x<0){
             drawX=0;
         }
         if(cam.getDirection().x>0){
             drawX=1;
         }
         if(cam.getDirection().y<0){
             drawY=0;
         }
         if(cam.getDirection().y>0){
             drawY=1;
         }
         if(cam.getDirection().z<0){
             drawZ=0;
         }
         if(cam.getDirection().z>0){
             drawZ=1;
         }
     };
    public Vector2f quadrant0(Vector3f playerLoc){
        float pX = playerLoc.x;
        float pZ = playerLoc.z;
        float chunkX = (pX-16)/16;
        float chunkZ = (pZ-16)/16;
        int chunkRX = (int) Math.floor(chunkX);
        int chunkRZ = (int) Math.floor(chunkZ);
        //System.out.println(chunkRX+":"+chunkRZ);
        return new Vector2f(chunkRX, chunkRZ);
    }
    public Vector2f quadrant1(Vector3f playerLoc){
        float pX = playerLoc.x;
        float pZ = playerLoc.z;
        float chunkX = pX/16;
        float chunkZ = (pZ-16)/16;
        int chunkRX = (int) Math.floor(chunkX);
        int chunkRZ = (int) Math.floor(chunkZ);
        //System.out.println(chunkRX+":"+chunkRZ);
        return new Vector2f(chunkRX, chunkRZ);
    }
    public Vector2f quadrant2(Vector3f playerLoc){
        float pX = playerLoc.x;
        float pZ = playerLoc.z;
        float chunkX = (pX+16)/16;
        float chunkZ = (pZ-16)/16;
        int chunkRX = (int) Math.floor(chunkX);
        int chunkRZ = (int) Math.floor(chunkZ);
        //System.out.println(chunkRX+":"+chunkRZ);
        return new Vector2f(chunkRX, chunkRZ);
    }
 
    @SuppressWarnings({"null", "ConstantConditions"})
    public Vector4f[] readChunk(Vector2f read){
        ChunkBuilder c = null;
        int x = (int)read.x;
        int z = (int)read.y;
        if(x>0 && z>0){
            try{
                FileInputStream fileIn = new FileInputStream(":"+Integer.toString(x)+":"+Integer.toString(z));
                ObjectInputStream in = new ObjectInputStream(fileIn);
                c = (ChunkBuilder) in.readObject();
                in.close();
                fileIn.close();
            }
            catch(IOException s){
                System.out.println("IOException");
            }
            catch(ClassNotFoundException e){
                System.out.println("ChunkBuilder class not found");
            }
            catch(NullPointerException e){
                System.out.println("Chunk Does not Exist");
            }
            return c.chunk;
        }
        return c.chunk;
    }
    
    public void renderQuadrant(Vector4f[] a){

        //if(drawX!=newdrawX){
            if(a!=null){
                for(int i=0; i<a.length;i++){
                    if(a[i]!=null){
                        geox = doChunkRenderingX(a[i], a);
                        geoxsub = doChunkRenderingXsub(a[i], a);
                        if(geox!=null){
                            blockx.attachChild(geox);
                        }
                        if(geoxsub!=null){
                            blockx.attachChild(geoxsub);
                        }
                    }
                }
                rootNode.attachChild(blockx);
            }
        //}
        //if(drawY!=newdrawY){
            if(a!=null){
                for(int i=0; i<a.length;i++){
                    if(a[i]!=null){
                        geoy = doChunkRenderingY(a[i], a);
                        geoysub = doChunkRenderingYsub(a[i], a);
                        if(geoy!=null){
                            blocky.attachChild(geoy);
                        }
                        if(geoysub!=null){
                            blocky.attachChild(geoysub);
                        }
                    }
                }
                //if(blocky.checkCulling(cam)==true){
                    rootNode.attachChild(blocky);
                //}
                
            }
        //}
        //if(drawZ!=newdrawZ){
            if(a!=null){
                for(int i=0; i<a.length;i++){
                    if(a[i]!=null){
                        geoz = doChunkRenderingZ(a[i], a);
                        geozsub = doChunkRenderingZsub(a[i], a);
                        if(geoz!=null){
                            blockz.attachChild(geoz);
                        }
                        if(geozsub!=null){
                            blockz.attachChild(geozsub);
                        }
                    }
                }
                //if(blockz.checkCulling(cam)==true){
                    rootNode.attachChild(blockz);
                //}
            }
        //}
     }
    
    /*
     * IN THE CASE THAT doChunkRendering() SHOULD BE LOOKING IN ANOTHER CHUNK
     * NEED TO PROVIDE IT A SECOND CHUNK TO CHECK IN AS WELL AS DEFINED CASES FOR WHEN TO LOOK IN THE BORDERING CHUNK
     */
    /*
    private Geometry doChunkRenderingX(Vector4f blockPos, Vector4f[] chunk){
         if((blockPos.w!=0)){
             if(getBlockID((int)blockPos.x+1, (int)blockPos.y, (int)blockPos.z, chunk)==0){
                 //if(cam.getDirection().x<0.05){
                    gx = Renderer.doRenderRight(blockPos);
                    Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
                    mat.setTexture("ColorMap", assetManager.loadTexture("Textures/Block/dirt.png"));
                    //mat.setTexture("ColorMap", assetManager.loadTexture("Textures/Block/"+GenericBlock.getTexture(((int)blockPos.getW()))+".png"));
                    gx.setMaterial(mat);    
                //}
            }
        }
         return gx;
    }
    private Geometry doChunkRenderingXsub(Vector4f blockPos, Vector4f[] chunk){
        if((blockPos.w!=0)){
            if(getBlockID((int)blockPos.x-1, (int)blockPos.y, (int)blockPos.z, chunk)==0){
                //if(cam.getDirection().x>-0.05){
                    gxsub = Renderer.doRenderLeft(blockPos);
                    Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
                    mat.setTexture("ColorMap", assetManager.loadTexture("Textures/Block/dirt.png"));
                    //mat.setTexture("ColorMap", assetManager.loadTexture("Textures/Block/"+GenericBlock.getTexture(((int)blockPos.getW()))+".png"));
                    gxsub.setMaterial(mat);
                //}
            }
        }
        return gxsub;
    }
    private Geometry doChunkRenderingY(Vector4f blockPos, Vector4f[] chunk){
         if(blockPos.w!=0){
             if(getBlockID((int)blockPos.x, (int)blockPos.y+1, (int)blockPos.z, chunk)==0){
                 //if((cam.getDirection().y<0.05)){
                     gy = Renderer.doRenderTop(blockPos);
                     Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
                     mat.setTexture("ColorMap", assetManager.loadTexture("Textures/Block/dirt.png"));
                     //mat.setTexture("ColorMap", assetManager.loadTexture("Textures/Block/"+GenericBlock.getTexture(((int)blockPos.getW()))+".png"));
                     gy.setMaterial(mat);
                 //}
             }
         }
         return gy;        
     }   
    private Geometry doChunkRenderingYsub(Vector4f blockPos, Vector4f[] chunk){
         if((blockPos.w!=0)){
             if(getBlockID((int)blockPos.x, (int)blockPos.y-1, (int)blockPos.z, chunk)==0){
                 //if((cam.getDirection().y>-0.05)){
                     gysub = Renderer.doRenderBot(blockPos);
                     Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
                     mat.setTexture("ColorMap", assetManager.loadTexture("Textures/Block/dirt.png"));
                     //mat.setTexture("ColorMap", assetManager.loadTexture("Textures/Block/"+GenericBlock.getTexture(((int)blockPos.getW()))+".png"));
                     gysub.setMaterial(mat);
                 //}
             }
         }
         return gysub;
     }
    private Geometry doChunkRenderingZ(Vector4f blockPos, Vector4f[] chunk){
         if(blockPos.w!=0){
             if(getBlockID((int)blockPos.x, (int)blockPos.y, (int)blockPos.z-1, chunk)==0){
                 //if(cam.getDirection().z>-0.05){
                     gz = Renderer.doRenderAway(blockPos);
                     Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
                     mat.setTexture("ColorMap", assetManager.loadTexture("Textures/Block/dirt.png"));
                     //mat.setTexture("ColorMap", assetManager.loadTexture("Textures/Block/"+GenericBlock.getTexture(((int)blockPos.getW()))+".png"));
                     gz.setMaterial(mat);
                 //}
             }
         }
         return gz;
     }
    private Geometry doChunkRenderingZsub(Vector4f blockPos, Vector4f[] chunk){
         if(blockPos.w!=0){
             if(getBlockID((int)blockPos.x, (int)blockPos.y, (int)blockPos.z+1, chunk)==0){
                 //if(cam.getDirection().z<0.05){
                     gzsub = Renderer.doRenderTowards(blockPos);
                     Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
                     mat.setTexture("ColorMap", assetManager.loadTexture("Textures/Block/dirt.png"));
                     //mat.setTexture("ColorMap", assetManager.loadTexture("Textures/Block/"+GenericBlock.getTexture(((int)blockPos.getW()))+".png"));
                     gzsub.setMaterial(mat);
                 //}
             }
         }
         return gzsub;
     } 
    */
     //if(drawX!=newdrawX){
            //blockx.detachAllChildren();
        //}
        //if(drawY!=newdrawY){
            //blocky.detachAllChildren();
        //}
        //if(drawZ!=newdrawZ){
            //blockz.detachAllChildren();
        //}

        /*if(oldCameraPos==null){
            //tempChunk = readChunk(quadrant0(cam.getLocation()));
            //tempChunk1 = readChunk(quadrant1(cam.getLocation()));
            //tempChunk2 = readChunk(quadrant2(cam.getLocation()));
        }
        
        if(oldCameraPos!=null){
            if(quadrant0(cam.getLocation()).y != oldCameraPos.y){
                drawX=-1;
                drawY=-1;
                drawZ=-1;
                //tempChunk = readChunk(quadrant0(cam.getLocation()));
                //tempChunk1 = readChunk(quadrant1(cam.getLocation()));
                //tempChunk2 = readChunk(quadrant2(cam.getLocation()));
                System.out.println("Chunk Position Updated");
            }
        }
        
        //if(doTemp==true){
            /*
             * NEED CODE TO TAKE PLAYER POSITION AND RENDER CHUNKS IN DETERMINED QUADRANTS
             * AS THE PLAYER MOVES THESE VALUES WILL CHANGE
             * IF THE CHUNK EXISTS ON DISK THEN WE SHOULD FIND AND RENDER IT
            */
        
        /*if(oldCameraPos==null){
            tempChunk = readChunk(quadrant0(cam.getLocation()));
            tempChunk1 = readChunk(quadrant1(cam.getLocation()));
            tempChunk2 = readChunk(quadrant2(cam.getLocation()));
        }
        
        if(oldCameraPos!=null){
            if(quadrant0(cam.getLocation()).y != oldCameraPos.y){
                tempChunk = readChunk(quadrant0(cam.getLocation()));
                tempChunk1 = readChunk(quadrant1(cam.getLocation()));
                tempChunk2 = readChunk(quadrant2(cam.getLocation()));
                drawX=-1;
                drawY=-1;
                drawZ=-1;
                System.out.println("Chunk Position Updated");
            }
        }*/

        //oldCameraPos = quadrant0(cam.getLocation());
            //tempChunk  = readChunk(quadrant0(cam.getLocation()));
            //tempChunk1 = readChunk(0,1);
            //tempChunk2 = readChunk(0,2);
            //tempChunk3 = readChunk(1,0);
            //tempChunk4 = readChunk(1,1);
            //tempChunk5 = readChunk(1,2);
            //tempChunk6 = readChunk(2,0);
            //tempChunk7 = readChunk(2,1);
            //tempChunk8 = readChunk(2,2);
        //}

        //renderQuadrant(tempChunk );
        //renderQuadrant(tempChunk1 );
        //renderQuadrant(tempChunk2 );
        //renderQuadrant(tempChunk1);
        //renderQuadrant(tempChunk2);
        //renderQuadrant(tempChunk3);
        //renderQuadrant(tempChunk4);
        //renderQuadrant(tempChunk5);
        //renderQuadrant(tempChunk6);
        //renderQuadrant(tempChunk7);
        //renderQuadrant(tempChunk8);
}
