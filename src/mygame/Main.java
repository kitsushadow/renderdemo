package mygame;
 
import com.jme3.app.SimpleApplication;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.math.Vector4f;
import com.jme3.renderer.RenderManager;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class Main extends SimpleApplication {
    
    public static boolean shouldUpdate=false;
    public static boolean updateZero, updateOne, updateTwo;
    public static boolean removeZero;
    Vector3f cameraPos = new Vector3f(33,5,33);
    public static Vector3f mainCamLocation;
    
    Vector2f[] chunksToLoad = new Vector2f[3];

    private QuadrantZeroState quadrantZeroState;
    private QuadrantOneState quadrantOneState;
   
    public static void main(String[] args) {
        Main app = new Main();
        app.start();
    };
    /*TAKES 3 COORDS AND A CHUNK ARRAY AND RETURNS U THE INTEGER ID VALUE FOR THE GIVEN COORDS*/
    public static Integer getBlockID(int x, int y, int z, Vector4f[] chunk){
        for(int index=0;index<chunk.length;index++){
            if(((chunk[index].x)==x)&&((chunk[index].y)==y)&&((chunk[index].z)==z)){
                return (int)chunk[index].w;
            }
        }
        return -1;
    };
    /*GETTER FOR THE PROTECTED CAMERA LOCATION*/
    public static Vector3f getCamLocation(){
        return mainCamLocation;
    };
    /*ADD KEY TRIGGERS*/
    private void initKeys(){
        inputManager.addMapping("Update",  new KeyTrigger(KeyInput.KEY_U));
        inputManager.addListener(actionListener,"Update");
    };
    /*WHAT THE KEY TRIGGERS DO*/
    private ActionListener actionListener = new ActionListener() {
        public void onAction(String name, boolean keyPressed, float tpf) {
            if (name.equals("Update") && !keyPressed) {
                shouldUpdate = true;
            }
        }
    };
 
    /*MAIN INIT METHOD RUNS ON STARTUP*/
    @Override
    public void simpleInitApp() {
        cam.setLocation(cameraPos);
        mainCamLocation = cam.getLocation();
        initKeys();
        flyCam.setMoveSpeed(10);
        /*ADD YOUR STATES HERE*/
        quadrantZeroState   = new QuadrantZeroState(this);
        /*ATTACH YOUR STATES HERE*/
        stateManager.attach(quadrantZeroState);
        
        /*
         * MAKES 9 VECTOR4 ARRAYS AND NAMES THE SERIALZED ARRAYS AS :X:Y:Z
         * VALUES WRITTEN TO DISK ARE:
         * CHUNK LOCATION /16
         * INT X 
         * INT Y
         * INT Z
         * VECTOR4[]
         */
           /* for(int x=0;x<50;x++){
                for(int z=0;z<50;z++){
                    ChunkBuilder e = new ChunkBuilder();
                    e.intX=x;
                    e.intY=0;
                    e.intZ=z;
                    Vector3f origin = new Vector3f(x*16,0,z*16);
                    e.chunk = e.makeChunk(origin);
                    try{
                        FileOutputStream fileOut = new FileOutputStream(":"+String.valueOf(x)+":"+String.valueOf(z));
                        ObjectOutputStream out = new ObjectOutputStream(fileOut);
                        out.writeObject(e);
                        out.close();
                        fileOut.close();
                        //System.out.println("Serialized data is saved in World");
                    }
                    catch(IOException s){
                        s.printStackTrace();
                    }
                }
            
        
        quadrantZeroState = new QuadrantZeroState(this);
        quadrantOneState = new QuadrantOneState(this);
        stateManager.attach(quadrantZeroState);
        stateManager.attach(quadrantOneState);
    }*/
 
    /*SIMPLE UPDATE LOOP*/
    /*@Override
    public void simpleUpdate(float tpf) {
        mainCamLocation = cam.getLocation();

        if(shouldUpdate == true){

        }
        shouldUpdate = false;
     }*/
 
    /* @Override
     public void simpleRender(RenderManager rm) {

    }*/
    }
}