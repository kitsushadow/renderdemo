/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.asset.AssetManager;
import com.jme3.material.Material;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.math.Vector4f;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

/**
 *
 * @author
 * kitsu
 */
public class QuadrantZeroState extends AbstractAppState{
    
    private SimpleApplication app;
    private AppStateManager stateManager;
    private ViewPort viewPort;
    private Node rootNode;
    private AssetManager assetManager;
    private Node localRootNode = new Node("Quadrant Zero RootNode");
    public Vector2f origin, chunkOrigin;
    private boolean moveChunk;
    public Vector4f[] tempChunk;
    public Vector2f playerOrigin0, chunkOrigin0;
    Geometry gy, gysub, gx, gxsub, gz, gzsub;
    Geometry geox, geoxsub, geoy, geoysub, geoz, geozsub;
    
  
    public QuadrantZeroState(SimpleApplication app){
    this.rootNode      = app.getRootNode();
    this.viewPort      = app.getViewPort();
    this.assetManager  = app.getAssetManager();
  }
    /*SETS THE LOCATION OF THE TOP RIGHT QUADRANT*/
    Vector2f quadrantZero(Vector3f playerLoc){
        float pX = playerLoc.x;
        float pZ = playerLoc.z;
        float chunkX = (pX-16)/16;
        float chunkZ = (pZ+16)/16;
        int chunkRX = (int) Math.floor(chunkX);
        int chunkRZ = (int) Math.floor(chunkZ);
        return new Vector2f(chunkRX, chunkRZ);
  };
    
    /*READS THE SERIALIZED CHUNK FILE*/
    @SuppressWarnings({"null", "ConstantConditions"})
    public Vector4f[] readChunk(Vector2f read){
        ChunkBuilder c = null;
        int x = (int)read.x;
        int z = (int)read.y;
        if(x>=0 && z>=0){
            try{
                FileInputStream fileIn = new FileInputStream(":"+Integer.toString(x)+":"+Integer.toString(z));
                ObjectInputStream in = new ObjectInputStream(fileIn);
                c = (ChunkBuilder) in.readObject();
                in.close();
                fileIn.close();
            }
            catch(IOException s){
                System.out.println("IOException");
            }
            catch(ClassNotFoundException e){
                System.out.println("ChunkBuilder class not found");
            }
            catch(NullPointerException e){
                System.out.println("Chunk Does not Exist");
            }
            return c.chunk;
        }
        return c.chunk;
    };
    
    /*READS THE DESERIALIZED CHUNK ARRAY AND ATTACHES THE MESH OBJECTS TO THE localRootNode*/
    public void renderQuadrant(Vector4f[] a){

        if(a!=null){
            for(int i=0; i<a.length;i++){
                if(a[i]!=null){
                    geox = doChunkRenderingX(a[i], a);
                    geoxsub = doChunkRenderingXsub(a[i], a);
                    geoy = doChunkRenderingY(a[i], a);
                    geoysub = doChunkRenderingYsub(a[i], a);
                    geoz = doChunkRenderingZ(a[i], a);
                    geozsub = doChunkRenderingZsub(a[i], a);
                    if(geox!=null){
                        localRootNode.attachChild(geox);
                    }
                    if(geoxsub!=null){
                        localRootNode.attachChild(geoxsub);
                    }
                    if(geoy!=null){
                        localRootNode.attachChild(geoy);
                    }
                    if(geoysub!=null){
                        localRootNode.attachChild(geoysub);
                    }
                    if(geoz!=null){
                        localRootNode.attachChild(geoz);
                    }
                    if(geozsub!=null){
                        localRootNode.attachChild(geozsub);
                    }
                }
            }
        }
    };
  
    /*DETERMINES WHICH BLOCKS TO RENDER AND NOT TO RENDER*/
    private Geometry doChunkRenderingX(Vector4f blockPos, Vector4f[] chunk){
        if((blockPos.w!=0)){
             if(Main.getBlockID((int)blockPos.x+1, (int)blockPos.y, (int)blockPos.z, chunk)==0){
                gx = Renderer.doRenderRight(blockPos);
                Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
                mat.setTexture("ColorMap", assetManager.loadTexture("Textures/Block/dirt.png"));
                //mat.setTexture("ColorMap", assetManager.loadTexture("Textures/Block/"+GenericBlock.getTexture(((int)blockPos.getW()))+".png"));
                gx.setMaterial(mat);    
            }
        }
        return gx;
    };
    private Geometry doChunkRenderingXsub(Vector4f blockPos, Vector4f[] chunk){
        if((blockPos.w!=0)){
            if(Main.getBlockID((int)blockPos.x-1, (int)blockPos.y, (int)blockPos.z, chunk)==0){
                gxsub = Renderer.doRenderLeft(blockPos);
                Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
                mat.setTexture("ColorMap", assetManager.loadTexture("Textures/Block/dirt.png"));
                //mat.setTexture("ColorMap", assetManager.loadTexture("Textures/Block/"+GenericBlock.getTexture(((int)blockPos.getW()))+".png"));
                gxsub.setMaterial(mat);
            }
        }
        return gxsub;
    };
    private Geometry doChunkRenderingY(Vector4f blockPos, Vector4f[] chunk){
         if(blockPos.w!=0){
             if(Main.getBlockID((int)blockPos.x, (int)blockPos.y+1, (int)blockPos.z, chunk)==0){
                 gy = Renderer.doRenderTop(blockPos);
                 Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
                 mat.setTexture("ColorMap", assetManager.loadTexture("Textures/Block/dirt.png"));
                 //mat.setTexture("ColorMap", assetManager.loadTexture("Textures/Block/"+GenericBlock.getTexture(((int)blockPos.getW()))+".png"));
                 gy.setMaterial(mat);
             }
         }
         return gy;        
     };   
    private Geometry doChunkRenderingYsub(Vector4f blockPos, Vector4f[] chunk){
         if((blockPos.w!=0)){
             if(Main.getBlockID((int)blockPos.x, (int)blockPos.y-1, (int)blockPos.z, chunk)==0){
                 gysub = Renderer.doRenderBot(blockPos);
                 Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
                 mat.setTexture("ColorMap", assetManager.loadTexture("Textures/Block/dirt.png"));
                 //mat.setTexture("ColorMap", assetManager.loadTexture("Textures/Block/"+GenericBlock.getTexture(((int)blockPos.getW()))+".png"));
                 gysub.setMaterial(mat);
             }
         }
         return gysub;
     };
    private Geometry doChunkRenderingZ(Vector4f blockPos, Vector4f[] chunk){
         if(blockPos.w!=0){
             if(Main.getBlockID((int)blockPos.x, (int)blockPos.y, (int)blockPos.z-1, chunk)==0){
                 gz = Renderer.doRenderAway(blockPos);
                 Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
                 mat.setTexture("ColorMap", assetManager.loadTexture("Textures/Block/dirt.png"));
                 //mat.setTexture("ColorMap", assetManager.loadTexture("Textures/Block/"+GenericBlock.getTexture(((int)blockPos.getW()))+".png"));
                 gz.setMaterial(mat);
             }
         }
         return gz;
     };
    private Geometry doChunkRenderingZsub(Vector4f blockPos, Vector4f[] chunk){
         if(blockPos.w!=0){
             if(Main.getBlockID((int)blockPos.x, (int)blockPos.y, (int)blockPos.z+1, chunk)==0){
                 gzsub = Renderer.doRenderTowards(blockPos);
                 Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
                 mat.setTexture("ColorMap", assetManager.loadTexture("Textures/Block/dirt.png"));
                 //mat.setTexture("ColorMap", assetManager.loadTexture("Textures/Block/"+GenericBlock.getTexture(((int)blockPos.getW()))+".png"));
                 gzsub.setMaterial(mat);
             }
         }
         return gzsub;
     };
    
    private Integer originMaker(Float i){
        return (int) Math.floor((double)(i/16));
    }
  
    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);
        this.app = (SimpleApplication)app;
        this.stateManager = (AppStateManager)stateManager;
        
    /*DO INITIAL CODE*/
        cleanup();
        if(chunkOrigin0==null){
            chunkOrigin0=quadrantZero(Main.getCamLocation());
        }
        System.out.println(chunkOrigin0);
        tempChunk = readChunk(chunkOrigin0);
        renderQuadrant(tempChunk );

        origin = new Vector2f (Main.getCamLocation().x,(Main.getCamLocation().z));
        playerOrigin0 = new Vector2f(originMaker(Main.getCamLocation().x),originMaker(Main.getCamLocation().z));
    };
 
    @Override
    public void stateAttached(AppStateManager stateManager) {
        rootNode.attachChild(localRootNode);
    };
 
    @Override
    public void stateDetached(AppStateManager stateManager) {
        cleanup();
        this.rootNode.detachAllChildren();
        rootNode.detachAllChildren();
        
    };
    
    @Override
    public void cleanup() {
        this.localRootNode.detachAllChildren();
    };
    
    @Override
    public void update(float tpf) {
        //System.out.println(Math.abs(chunkOrigin0.x-originMaker(Main.getCamLocation().x)));
        if(Math.abs(chunkOrigin0.x-originMaker(Main.getCamLocation().x))==3){
            System.out.println("Player Range Reached");
            System.out.println((playerOrigin0.x-originMaker(Main.getCamLocation().x)));
            
            if((playerOrigin0.x-originMaker(Main.getCamLocation().x))<0){
                System.out.println("Ready for update");
                chunkOrigin0 = new Vector2f((originMaker(Main.getCamLocation().x)+2),originMaker(Main.getCamLocation().z)+1);
                playerOrigin0 = new Vector2f(originMaker(Main.getCamLocation().x), originMaker(Main.getCamLocation().z));
                this.initialize(stateManager, app);
            }
            if((playerOrigin0.x-originMaker(Main.getCamLocation().x))>0){
                System.out.println("Ready for update");
                chunkOrigin0 = new Vector2f((originMaker(Main.getCamLocation().x)-2),originMaker(Main.getCamLocation().z)+1);
                playerOrigin0 = new Vector2f(originMaker(Main.getCamLocation().x), originMaker(Main.getCamLocation().z));
                this.initialize(stateManager, app);
            }
        }
        
        if(Math.abs(chunkOrigin0.y-originMaker(Main.getCamLocation().z))==3){
            System.out.println("Player Range Reached");
            System.out.println((playerOrigin0.x-originMaker(Main.getCamLocation().x)));
            
            if((playerOrigin0.y-originMaker(Main.getCamLocation().z))<0){
                System.out.println("Ready for update");
                chunkOrigin0 = new Vector2f((originMaker(Main.getCamLocation().x)+2),originMaker(Main.getCamLocation().z)+1);
                playerOrigin0 = new Vector2f(originMaker(Main.getCamLocation().x), originMaker(Main.getCamLocation().z));
                this.initialize(stateManager, app);
            }
            if((playerOrigin0.y-originMaker(Main.getCamLocation().z))>0){
                System.out.println("Ready for update");
                chunkOrigin0 = new Vector2f((originMaker(Main.getCamLocation().x)-2),originMaker(Main.getCamLocation().z)+1);
                playerOrigin0 = new Vector2f(originMaker(Main.getCamLocation().x), originMaker(Main.getCamLocation().z));
                this.initialize(stateManager, app);
            }
        }

        Vector2f playerLoc = new Vector2f((Main.getCamLocation().x),(Main.getCamLocation().z));

    
    };
    
    
}
