/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.math.Vector4f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Mesh;
import com.jme3.scene.VertexBuffer.Type;
import com.jme3.util.BufferUtils;

/**
 *
 * @author kitsu
 */
public class Renderer{
  
    
    /*
* y Positive Face = Side 0 TOP             0,-1, 0
* y Negative Face = Side 1 BOTTOM          0, 1, 0
* x Positive Face = Side 2 RIGHT          -1, 0, 0
* x Negative Face = Side 3 LEFT            1, 0, 0
* z Positive Face = Side 4 FACING TOWARDS  0, 0, -1
* z Negative Face = Side 5 FACING AWAY     0, 0, 1
* 
* while y < 0 render side 0
* while y > 0 render side 1
* while x < 0 render side 2
* while x > 0 render side 3
* while z < 0 render side 4
* while z > 0 render side 5
* 
* Break Camera Vector into X, Y, Z
* If statements for x y and z which face to render
* Make Vertexes in reference to the block vector coord
* 
* 
* 
Render a face at pos
Use cam to determine orientation of the face
*/
    
    /*
     TODO
     * And a renderer for side 0 & 1 at the same time
     * And a renderer for side 2 & 3 at the same time
     * And a renderer for side 4 & 5 at the same time
     */
  
  //Renders Side 0
  public static Geometry doRenderTop(Vector4f blockPos){
      float x = blockPos.x;
      float y = blockPos.y;
      float z = blockPos.z;
      
    Mesh mesh = new Mesh();
    Vector3f [] verts = new Vector3f[4];

    verts[0] = new Vector3f(x-0.5f,y+0.5f,z+0.5f);
    verts[1] = new Vector3f(x+0.5f,y+0.5f,z+0.5f);
    verts[2] = new Vector3f(x-0.5f,y+0.5f,z-0.5f);
    verts[3] = new Vector3f(x+0.5f,y+0.5f,z-0.5f);
    
    Vector2f[] texCoord = new Vector2f[4];
    texCoord[0] = new Vector2f(0,0);
    texCoord[1] = new Vector2f(1,0);
    texCoord[2] = new Vector2f(0,1);
    texCoord[3] = new Vector2f(1,1);

    int [] indexes = {2,0,1,1,3,2};
    //int [] indexes = {2,3,1,1,0,2};

    mesh.setBuffer(Type.Position, 3, BufferUtils.createFloatBuffer(verts));
    mesh.setBuffer(Type.TexCoord, 2, BufferUtils.createFloatBuffer(texCoord));
    mesh.setBuffer(Type.Index,    3, BufferUtils.createIntBuffer(indexes));
    mesh.updateBound();
    Geometry geo = new Geometry("TopMesh", mesh);
    return geo;
  }
  //Renders Side 1
    public static Geometry doRenderBot(Vector4f blockPos){
        float x = blockPos.x;
        float y = blockPos.y;
        float z = blockPos.z;
    Mesh mesh = new Mesh();
    Vector3f [] verts = new Vector3f[4];

      verts[0] = new Vector3f(x-0.5f,y-0.5f,z+0.5f);
      verts[1] = new Vector3f(x+0.5f,y-0.5f,z+0.5f);
      verts[2] = new Vector3f(x-0.5f,y-0.5f,z-0.5f);
      verts[3] = new Vector3f(x+0.5f,y-0.5f,z-0.5f);
    
    
    Vector2f[] texCoord = new Vector2f[4];
    texCoord[0] = new Vector2f(0,0);
    texCoord[1] = new Vector2f(1,0);
    texCoord[2] = new Vector2f(0,1);
    texCoord[3] = new Vector2f(1,1);

    //int [] indexes = {2,0,1,1,3,2};
    int [] indexes = {2,3,1,1,0,2};

    mesh.setBuffer(Type.Position, 3, BufferUtils.createFloatBuffer(verts));
    mesh.setBuffer(Type.TexCoord, 2, BufferUtils.createFloatBuffer(texCoord));
    mesh.setBuffer(Type.Index,    3, BufferUtils.createIntBuffer(indexes));
    mesh.updateBound();
    Geometry geo = new Geometry("BotMesh", mesh);
    return geo;
  }
  //Renders Side 2
    public static Geometry doRenderRight(Vector4f blockPos){
        float x = blockPos.x;
        float y = blockPos.y;
        float z = blockPos.z;
    Mesh mesh = new Mesh();
    Vector3f [] verts = new Vector3f[4];

    verts[0] = new Vector3f(x+0.5f,y-0.5f,z+0.5f);
    verts[1] = new Vector3f(x+0.5f,y-0.5f,z-0.5f);
    verts[2] = new Vector3f(x+0.5f,y+0.5f,z+0.5f);
    verts[3] = new Vector3f(x+0.5f,y+0.5f,z-0.5f);
    
    
    Vector2f[] texCoord = new Vector2f[4];
    texCoord[0] = new Vector2f(0,0);
    texCoord[1] = new Vector2f(1,0);
    texCoord[2] = new Vector2f(0,1);
    texCoord[3] = new Vector2f(1,1);

    int [] indexes = {2,0,1,1,3,2};
    //int [] indexes = {2,3,1,1,0,2};

    mesh.setBuffer(Type.Position, 3, BufferUtils.createFloatBuffer(verts));
    mesh.setBuffer(Type.TexCoord, 2, BufferUtils.createFloatBuffer(texCoord));
    mesh.setBuffer(Type.Index,    3, BufferUtils.createIntBuffer(indexes));
    mesh.updateBound();
    Geometry geo = new Geometry("RightMesh", mesh);
    return geo;
    
  }
  //Renders Side 3
  public static Geometry doRenderLeft(Vector4f blockPos){
        float x = blockPos.x;
        float y = blockPos.y;
        float z = blockPos.z;
        Mesh mesh = new Mesh();
        Vector3f [] verts = new Vector3f[4];

        verts[0] = new Vector3f(x-0.5f,y-0.5f,z+0.5f);
        verts[1] = new Vector3f(x-0.5f,y-0.5f,z-0.5f);
        verts[2] = new Vector3f(x-0.5f,y+0.5f,z+0.5f);
        verts[3] = new Vector3f(x-0.5f,y+0.5f,z-0.5f);
    
    Vector2f[] texCoord = new Vector2f[4];
    texCoord[0] = new Vector2f(0,0);
    texCoord[1] = new Vector2f(1,0);
    texCoord[2] = new Vector2f(0,1);
    texCoord[3] = new Vector2f(1,1);

    //int [] indexes = {2,0,1,1,3,2};
   
    int [] indexes = {2,3,1,1,0,2};

    mesh.setBuffer(Type.Position, 3, BufferUtils.createFloatBuffer(verts));
    mesh.setBuffer(Type.TexCoord, 2, BufferUtils.createFloatBuffer(texCoord));
    mesh.setBuffer(Type.Index,    3, BufferUtils.createIntBuffer(indexes));
    mesh.updateBound();
    Geometry geo = new Geometry("LeftMesh", mesh);
    return geo;
    
  }
  //Renders Side 4
  
  public static Geometry doRenderTowards(Vector4f blockPos){
        float x = blockPos.x;
        float y = blockPos.y;
        float z = blockPos.z;
    Mesh mesh = new Mesh();
    Vector3f [] verts = new Vector3f[4];

    verts[0] = new Vector3f(x-0.5f,y-0.5f,z+0.5f);
    verts[1] = new Vector3f(x+0.5f,y-0.5f,z+0.5f);
    verts[2] = new Vector3f(x-0.5f,y+0.5f,z+0.5f);
    verts[3] = new Vector3f(x+0.5f,y+0.5f,z+0.5f);
    
    
    Vector2f[] texCoord = new Vector2f[4];
    texCoord[0] = new Vector2f(0,0);
    texCoord[1] = new Vector2f(1,0);
    texCoord[2] = new Vector2f(0,1);
    texCoord[3] = new Vector2f(1,1);

    int [] indexes = {2,0,1,1,3,2};
    //int [] indexes = {2,3,1,1,0,2};

    mesh.setBuffer(Type.Position, 3, BufferUtils.createFloatBuffer(verts));
    mesh.setBuffer(Type.TexCoord, 2, BufferUtils.createFloatBuffer(texCoord));
    mesh.setBuffer(Type.Index,    3, BufferUtils.createIntBuffer(indexes));
    mesh.updateBound();
    Geometry geo = new Geometry("Front", mesh);
    return geo;
  
  }
  
  //Renders Side 5
  public static Geometry doRenderAway(Vector4f blockPos){
        float x = blockPos.x;
        float y = blockPos.y;
        float z = blockPos.z;
    Mesh mesh = new Mesh();
    Vector3f [] verts = new Vector3f[4];

      verts[0] = new Vector3f(x-0.5f,y-0.5f,z-0.5f);
      verts[1] = new Vector3f(x+0.5f,y-0.5f,z-0.5f);
      verts[2] = new Vector3f(x-0.5f,y+0.5f,z-0.5f);
      verts[3] = new Vector3f(x+0.5f,y+0.5f,z-0.5f);
    
    Vector2f[] texCoord = new Vector2f[4];
    texCoord[0] = new Vector2f(0,0);
    texCoord[1] = new Vector2f(1,0);
    texCoord[2] = new Vector2f(0,1);
    texCoord[3] = new Vector2f(1,1);

    int [] indexes = {2,3,1,1,0,2};

    mesh.setBuffer(Type.Position, 3, BufferUtils.createFloatBuffer(verts));
    mesh.setBuffer(Type.TexCoord, 2, BufferUtils.createFloatBuffer(texCoord));
    mesh.setBuffer(Type.Index,    3, BufferUtils.createIntBuffer(indexes));
    mesh.updateBound();
    Geometry geo = new Geometry("BackMesh", mesh);
    return geo;
  }

    


}
/*
 
       Vector3f [] verts = new Vector3f[4];
                            //x,y,z
      //Front Face
      //Bottom Left
      verts[0] = new Vector3f(-0.5f,-0.5f,0.5f);
      //Bottom Right
      verts[1] = new Vector3f(0.5f,-0.5f,0.5f);
      //Top Left
      verts[2] = new Vector3f(-0.5f,0.5f,0.5f);
      //Top Right
      verts[3] = new Vector3f(0.5f,0.5f,0.5f);
      
      //Back Face
      //Bottom Left
      verts[4] = new Vector3f(-0.5f,-0.5f,-0.5f);
      //Bottom Right
      verts[5] = new Vector3f(0.5f,-0.5f,-0.5f);
      //Top Left
      verts[6] = new Vector3f(-0.5f,0.5f,-0.5f);
      //Top Right
      verts[7] = new Vector3f(0.5f,0.5f,-0.5f);
      
      //Right Face
      //Bottom Left
      verts[8] = new Vector3f(0.5f,-0.5f,0.5f);
      //Bottom Right
      verts[9] = new Vector3f(0.5f,-0.5f,-0.5f);
      //Top Left
      verts[10] = new Vector3f(0.5f,0.5f,0.5f);
      //Top Right
      verts[11] = new Vector3f(0.5f,0.5f,-0.5f);
      
      //Left Face
      //Bottom Left
      verts[12] = new Vector3f(-0.5f,-0.5f,0.5f);
      //Bottom Right
      verts[13] = new Vector3f(-0.5f,-0.5f,-0.5f);
      //Top Left
      verts[14] = new Vector3f(-0.5f,0.5f,0.5f);
      //Top Right
      verts[15] = new Vector3f(-0.5f,0.5f,-0.5f);
      
      //Top Face
      //Bottom Left
      verts[16] = new Vector3f(-0.5f,0.5f,0.5f);
      //Bottom Right
      verts[17] = new Vector3f(0.5f,0.5f,0.5f);
      //Top Left
      verts[18] = new Vector3f(-0.5f,0.5f,-0.5f);
      //Top Right
      verts[19] = new Vector3f(0.5f,0.5f,-0.5f);
      
       //Bottom Face
      //Bottom Left
      verts[20] = new Vector3f(-0.5f,-0.5f,0.5f);
      //Bottom Right
      verts[21] = new Vector3f(0.5f,-0.5f,0.5f);
      //Top Left
      verts[22] = new Vector3f(-0.5f,-0.5f,-0.5f);
      //Top Right
      verts[23] = new Vector3f(0.5f,-0.5f,-0.5f);
 
 
 */